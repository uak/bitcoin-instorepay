#!/usr/bin/env python3
# -*- mode: python3 -*-
#
# inStorePay - lightweight Bitcoin client
# Copyright (C) 2020 uak@gitlab
#
# This is free software licensed under AGPL v3
# 
# based on code from Sagnik Chattopadhyaya https://github.com/sagnik20/QR-Code-Generator
# Using Fountainhead.cash's Bitsocket for instant 0-conf payment confirmation



from tkinter import Button, Label, Entry, Tk, StringVar, DoubleVar, IntVar, PhotoImage, BitmapImage  ## u
from bitcash.network import currency_to_satoshi_cached
import os
import pyqrcode
import base64   
import json        
import sseclient
import threading

window = Tk()
window.title("Merchant App")
# starting postion of the window
window.geometry('+870+100')

amount_fiat = DoubleVar()
amount_satoshi = IntVar()

bch_address = 'qzg7thapl2p5cm2jrl5s7qelg8tvune3uylcjyttd3'
bch_address_prefix = 'bitcoincash:'+bch_address

success_image = PhotoImage(file='assets/success.png')


# Url of the bitsocket API using SSE (server side event)
BITSOCKET_URL = 'https://bitsocket.bch.sx/s/'


def query_bitsocket(query, fn):
  # convert query from python format to json 
  json_string = bytes(json.dumps(query), 'utf-8')
  url = BITSOCKET_URL + base64.b64encode(json_string).decode('utf-8')
  # Using python SSE library
  client = sseclient.SSEClient(url)
  global j
  for evt in client:
      try:
        #getting response from bitsocket server
        j = json.loads(evt.data)
        # excuting bitsocket_handler as the second argument of this function
        fn(j)   
      except Exception as e:
        print(e)                                                                               
        continue

# here it does compare the response from server with the amount it should get as specified in the QRcode
# and prints success if it matches and changes the QRcode image to show check mark

def bitsocket_handler(j):
  recieved = j[ 'data'][0]['value']
  if recieved == amount_satoshi:
	  print('success')
	  image_label.config(image = success_image )
  elif recieved != amount_satoshi:
	  print('Wrong amount sent')

# the query sent to Bitsocket is handled here inside query_bitsocket
# v for protocol verison
# q for query
# find searches for out.e.a which is an output address, 'limit' limits the querey to 10 results
# Project is to filter database response to show only first result from out.e
#  r f is to filter results of search, select make sure the reciving address .out .e .a is exactly the one specified. 
# {value: .v} make sure the response is returned in dictionary format, .v is for satoshis recieved to the address

def query_execute():
	query_bitsocket({
	  "v": 3,
	  "q": {
	    "find": {"out.e.a": "qzg7thapl2p5cm2jrl5s7qelg8tvune3uylcjyttd3"},
	    "limit": 10
	    ,
	    "project": {
	      "out.e": 1
	    }
	  },
	  "r": {
	    "f": "[.[] | .out[0] | .e  | select(.a == \"qzg7thapl2p5cm2jrl5s7qelg8tvune3uylcjyttd3\") | . | {value: .v}]"
	  }
	}, bitsocket_handler)



def generate_qrcode():
	# Using global to be able to use variable outside the function 
    global bip21
    global amount_satoshi
    # Getting USD price in satoshis using bitcash lib
    amount_satoshi = currency_to_satoshi_cached(amount_fiat.get(), 'usd')
    # Amount in BCH
    amount_bch = amount_satoshi/100000000
    # Generating bip21 payment request
    bip21 = bch_address_prefix+"?amount="+str(amount_bch)
    print(amount_satoshi)
    if len(bch_address)!=0 :
        global qr,photo
        # create qr code from variable bip21
        qr = pyqrcode.create(bip21)
        photo = BitmapImage(data = qr.xbm(scale=8))
    try:
        showcode()
    except:
        pass

## Function to show QRcode
def showcode():
    image_label.config(image = photo)
    sub_label.config(text="QR of " + bip21)

# Top Message on window
Sub = Label(window,text="Welcome")
Sub.pack()

# Amount label
amount_label = Label(window,text="Enter amount $")
amount_label.pack()

# Field where user enter the amount in fiat currency
amount_entry = Entry(window, justify='center', textvariable = amount_fiat)
amount_entry.pack()

# Button to get QR code
button = Button(window,text = "Get QR Code",width=15,command = generate_qrcode)
button.pack()

# Images shows in this label
image_label = Label(window)
image_label.pack()

# Text under the QR code for showing the full link with address
sub_label = Label(window,text="")
sub_label.pack()

# Starting a new thread for querying bitsocket so we don't freez window while waiting for
# results from source
threading.Thread(target=query_execute).start()
 
window.mainloop()



