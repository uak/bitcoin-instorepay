# Bitcoin InStorePay

Bitcoin Merchant App allows you to specify fiat amount and show customer a QRcode with Instant notification when payment received.
It's written in Python.

**Ingredients**

I've used some awesome tools to build this application.

1. Python
1. Tkinter GUI
1. Bitcash python Lib
1. Fountainhead Bitsocket API for instant notifications
1. QR-Code-Generator by Sagnik Chattopadhyaya


**Known bugs**

- Some results from the API aren't handled very well causing some harmless noise
- Bitcoin Cash address should be configured manually in 3 places
- Probably a lot more, since this is my first app


**Wish list**

- BIP70 support
- Onscreen Keypad
- SLP tokens support
- Multiple currencies
- Using multiple APIs
- Provide binary packages for major OSes
- Building for Mobile phones
- Add window to setup configurations
- Add xpub support

**Screenshot**


![](https://gitlab.com/uak/bitcoin-instorepay/-/raw/master/assets/instorepay-display.gif)

**License**

AGPL v3